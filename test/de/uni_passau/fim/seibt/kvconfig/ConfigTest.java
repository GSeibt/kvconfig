/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Georg Seibt
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.uni_passau.fim.seibt.kvconfig;

import java.io.File;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Optional;

import de.uni_passau.fim.seibt.kvconfig.sources.ConfigSource;
import de.uni_passau.fim.seibt.kvconfig.sources.MockConfigSource;
import org.junit.Before;
import org.junit.Test;

import static de.uni_passau.fim.seibt.kvconfig.sources.MockConfigSource.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ConfigTest {

    private static final String NONEXISTENT_KEY = "NONEXISTENT";

    private Config config;

    @Before
    public void setUp() throws Exception {
        config = new Config();
        config.addSource(new MockConfigSource(0));
    }

    @Test
    public void testAddSource() throws Exception {
        Field sourcesField = Config.class.getDeclaredField("sources");
        sourcesField.setAccessible(true);

        @SuppressWarnings("unchecked")
        List<ConfigSource> sources = (List<ConfigSource>) sourcesField.get(config);

        MockConfigSource s1 = new MockConfigSource(1);
        MockConfigSource s2 = new MockConfigSource(2);

        config.addSource(s1);
        config.addSource(s2);

        assertEquals(sources.size(), 3);
        assertTrue(sources.contains(s1));
        assertTrue(sources.contains(s2));
    }

    @Test
    public void testGet() throws Exception {
        Optional<String> exOpt = config.get(EXISTING_KEY);
        Optional<String> nonExOpt = config.get(NONEXISTENT_KEY);

        assertTrue(exOpt.isPresent());
        assertFalse(nonExOpt.isPresent());

        config.addSource(new MockConfigSource(1));
        config.addSource(new MockConfigSource(2));

        Optional<String> priorityKey = config.get(PRIORITY_KEY);

        assertTrue(priorityKey.isPresent());
        assertTrue(Integer.parseInt(priorityKey.get()) == 2);
    }

    @Test
    public void testGetInteger() throws Exception {
        Optional<Integer> intOpt = config.getInteger(INTEGER_KEY);
        Optional<Integer> parseFailOpt = config.getInteger(NONSENSE_KEY);

        assertTrue(intOpt.isPresent());
        assertTrue(intOpt.get() == Integer.parseInt(INTEGER_VALUE));
        assertFalse(parseFailOpt.isPresent());
    }

    @Test
    public void testGetLong() throws Exception {
        Optional<Long> longOpt = config.getLong(LONG_KEY);
        Optional<Long> parseFailOpt = config.getLong(NONSENSE_KEY);

        assertTrue(longOpt.isPresent());
        assertTrue(longOpt.get() == Long.parseLong(LONG_VALUE));
        assertFalse(parseFailOpt.isPresent());
    }

    @Test
    public void testGetFloat() throws Exception {
        Optional<Float> floatOpt = config.getFloat(FLOAT_KEY);
        Optional<Float> parseFailOpt = config.getFloat(NONSENSE_KEY);

        assertTrue(floatOpt.isPresent());
        assertTrue(floatOpt.get() == Float.parseFloat(FLOAT_VALUE));
        assertFalse(parseFailOpt.isPresent());
    }

    @Test
    public void testGetDouble() throws Exception {
        Optional<Double> doubleOpt = config.getDouble(DOUBLE_KEY);
        Optional<Double> parseFailOpt = config.getDouble(NONSENSE_KEY);

        assertTrue(doubleOpt.isPresent());
        assertTrue(doubleOpt.get() == Double.parseDouble(DOUBLE_VALUE));
        assertFalse(parseFailOpt.isPresent());
    }

    @Test
    public void testGetBoolean() throws Exception {
        Optional<Boolean> boolOpt = config.getBoolean(BOOLEAN_KEY);
        Optional<Boolean> parseFailOpt = config.getBoolean(NONSENSE_KEY);

        assertTrue(boolOpt.isPresent());
        assertTrue(boolOpt.get());

        assertTrue(parseFailOpt.isPresent());
        assertFalse(parseFailOpt.get());
    }

    @Test
    public void testGetFile() throws Exception {
        Optional<File> fileOpt = config.getFile(FILE_KEY);

        assertTrue(fileOpt.isPresent());
        assertTrue("Could not delete the File returned by the MockConfigSource.", fileOpt.get().delete());

        fileOpt = config.getFile(FILE_KEY);

        assertTrue(fileOpt.isPresent());
        assertFalse(fileOpt.get().exists());
    }

    @Test
    public void testGetExistingFile() throws Exception {
        Optional<File> fileOpt = config.getExistingFile(FILE_KEY);

        assertTrue(fileOpt.isPresent());
        assertTrue("Could not delete the File returned by the MockConfigSource.", fileOpt.get().delete());

        fileOpt = config.getExistingFile(FILE_KEY);

        assertFalse(fileOpt.isPresent());
    }

    @Test
    public void testGetEnum() throws Exception {
        Optional<TestEnum> enumOpt = config.getEnum(ENUM_KEY, TestEnum.class);
        Optional<TestEnum> parseFailOpt = config.getEnum(NONSENSE_KEY, TestEnum.class);

        assertTrue(enumOpt.isPresent());
        assertEquals(enumOpt.get(), TestEnum.TEST);

        assertFalse(parseFailOpt.isPresent());
    }
}