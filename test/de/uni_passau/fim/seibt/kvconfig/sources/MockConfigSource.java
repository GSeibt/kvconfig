/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Georg Seibt
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.uni_passau.fim.seibt.kvconfig.sources;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.util.Optional;

import de.uni_passau.fim.seibt.kvconfig.ConfigTest;

/**
 * A mock <code>ConfigSource</code>. The {@link #get(String)} method returns sample values listed below. <br>
 *
 * <table>
 *  <tr>
 *      <th>Key</th>
 *      <th>Value</th>
 *  </tr>
 *  <tr>
 *    <td>{@value #EXISTING_KEY}</td>
 *    <td>{@value #EXISTING_VALUE}</td>
 *  </tr>
 *  <tr>
 *    <td>{@value #NONSENSE_KEY}</td>
 *    <td>{@value #NONSENSE_VALUE}</td>
 *  </tr>
 *  <tr>
 *    <td>{@value #INTEGER_KEY}</td>
 *    <td>{@value #INTEGER_VALUE}</td>
 *  </tr>
 *  <tr>
 *    <td>{@value #LONG_KEY}</td>
 *    <td>{@value #LONG_VALUE}</td>
 *  </tr>
 *  <tr>
 *    <td>{@value #FLOAT_KEY}</td>
 *    <td>{@value #FLOAT_VALUE}</td>
 *  </tr>
 *  <tr>
 *    <td>{@value #DOUBLE_KEY}</td>
 *    <td>{@value #DOUBLE_VALUE}</td>
 *  </tr>
 *  <tr>
 *    <td>{@value #BOOLEAN_KEY}</td>
 *    <td>{@value #BOOLEAN_VALUE}</td>
 *  </tr>
 *  <tr>
 *    <td>{@value #PRIORITY_KEY}</td>
 *    <td>The priority as a <code>String</code>.</td>
 *  </tr>
 *  <tr>
 *    <td>{@value #FILE_KEY}</td>
 *    <td>A path to a temporary file.</td>
 *  </tr>
 *  <tr>
 *    <td>{@value #ENUM_KEY}</td>
 *    <td>{@value #ENUM_VALUE}</td>
 *  </tr>
 * </table>
 */
public class MockConfigSource extends ConfigSource {

    public static final String EXISTING_KEY = "EXISTING";
    public static final String EXISTING_VALUE = "EXISTING_KEY";

    public static final String NONSENSE_KEY = "NONSENSE";
    public static final String NONSENSE_VALUE = "!�$%&/()=?`�";

    public static final String INTEGER_KEY = "INTEGER";
    public static final String INTEGER_VALUE = "-12345";

    public static final String LONG_KEY = "LONG";
    public static final String LONG_VALUE = "9223372036854775000";

    public static final String FLOAT_KEY = "FLOAT";
    public static final String FLOAT_VALUE = "0x1.fffffep127";

    public static final String DOUBLE_KEY = "DOUBLE";
    public static final String DOUBLE_VALUE = "0x1.fffffffffffffp1023";

    public static final String BOOLEAN_KEY = "BOOLEAN";
    public static final String BOOLEAN_VALUE = "true";

    public static final String PRIORITY_KEY = "PRIORITY";

    public static final String FILE_KEY = "FILE";
    private File file;

    public static final String ENUM_KEY = "ENUM";
    public static final String ENUM_VALUE = "TEST";
    public enum TestEnum { TEST }

    public MockConfigSource() {}

    public MockConfigSource(int priority) {
        super(priority);
    }

    @Override
    protected Optional<String> getMapping(String key) {

        switch (key) {
            case EXISTING_KEY:
                return Optional.of(EXISTING_VALUE);
            case NONSENSE_KEY:
                return Optional.of(NONSENSE_VALUE);
            case INTEGER_KEY:
                return Optional.of(INTEGER_VALUE);
            case LONG_KEY:
                return Optional.of(LONG_VALUE);
            case FLOAT_KEY:
                return Optional.of(FLOAT_VALUE);
            case DOUBLE_KEY:
                return Optional.of(DOUBLE_VALUE);
            case BOOLEAN_KEY:
                return Optional.of(BOOLEAN_VALUE);
            case PRIORITY_KEY:
                try {
                    Field pr = ConfigSource.class.getDeclaredField("priority");
                    pr.setAccessible(true);

                    return Optional.of(String.valueOf(pr.getInt(this)));
                } catch (NoSuchFieldException | IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            case FILE_KEY:

                if (file == null) {
                    try {
                        file = Files.createTempFile(ConfigTest.class.getSimpleName(), null).toFile();
                        file.deleteOnExit();
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }

                return Optional.of(file.getAbsolutePath());
            case ENUM_KEY:
                return Optional.of(ENUM_VALUE);
            default:
                return Optional.empty();
        }
    }
}
