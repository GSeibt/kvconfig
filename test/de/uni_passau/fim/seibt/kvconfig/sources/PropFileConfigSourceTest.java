/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Georg Seibt
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.uni_passau.fim.seibt.kvconfig.sources;

import de.uni_passau.fim.seibt.kvconfig.Config;
import org.junit.Test;

import java.io.File;
import java.io.FileWriter;
import java.nio.file.Files;
import java.util.Optional;
import java.util.Properties;

import static org.junit.Assert.*;

public class PropFileConfigSourceTest {

    private static final String KEY = "KEY";
    private static final String VALUE = "VALUE";

    @Test
    public void testGet() throws Exception {
        Config config = new Config();
        Properties prop = new Properties();

        prop.setProperty(KEY, VALUE);
        config.addSource(new PropFileConfigSource(prop));

        Optional<String> opt = config.get(KEY);

        assertTrue(opt.isPresent());
        assertEquals(opt.get(), VALUE);

        opt = config.get(KEY.toLowerCase());

        assertFalse(opt.isPresent());
    }

    @Test
    public void testGet2() throws Exception {
        Config config = new Config();
        Properties prop = new Properties();

        prop.setProperty(KEY, VALUE);

        File propFile = Files.createTempFile(PropFileConfigSourceTest.class.getSimpleName(), null).toFile();

        try (FileWriter w = new FileWriter(propFile)) {
            prop.store(w, null);
        }

        try {
            config.addSource(new PropFileConfigSource(propFile));
        } finally {
            if (!propFile.delete()) {
                System.err.println("Could not delete " + propFile.getAbsolutePath());
                propFile.deleteOnExit();
            }
        }

        Optional<String> opt = config.get(KEY);

        assertTrue(opt.isPresent());
        assertEquals(opt.get(), VALUE);

        opt = config.get(KEY.toLowerCase());

        assertFalse(opt.isPresent());
    }
}