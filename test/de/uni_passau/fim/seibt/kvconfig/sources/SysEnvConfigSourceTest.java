/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Georg Seibt
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.uni_passau.fim.seibt.kvconfig.sources;

import de.uni_passau.fim.seibt.kvconfig.Config;
import org.junit.Test;

import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SysEnvConfigSourceTest {

    @Test
    public void testGet() throws Exception {
        Config config = new Config();
        Map<String, String> env = System.getenv();

        config.addSource(new SysEnvConfigSource());

        if (env.isEmpty()) {
            throw new AssertionError("Empty system environment - can not test " + SysEnvConfigSource.class.getSimpleName());
        }

        for (Map.Entry<String, String> entry : env.entrySet()) {
            Optional<String> opt = config.get(entry.getKey());

            assertTrue(opt.isPresent());
            assertEquals(opt.get(), entry.getValue());
        }
    }
}