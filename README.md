# KVConfig #

KVConfig is a tiny library for accessing *key*=*value* style config options. The value for a key can be retrieved from a `Config` that maintains an extendable collection of `ConfigSource` instances. Implementations of the `ConfigSource` abstract class that access properties files and environment variables are provided by the library. Others can be added easily.

The library supports returning the retrieved values parsed as all standard java types such as `Integer, Long, Float, Double` or as a `File`. Users may also use their own parsers. Instead of returning `null` the `Optional` class is used enabling clean handling of missing config values.

## Building ##

KVConfig uses [Gradle](https://gradle.org/) for building. Use `gradle build` to assemble a jar file containing all library classes in the `build\libs` directory.