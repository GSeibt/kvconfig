/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Georg Seibt
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.uni_passau.fim.seibt.kvconfig.sources;

import java.util.Optional;

/**
 * A <code>ConfigSource</code> that queries the environment variables using {@link System#getenv(String)} when
 * {@link #get(String)} is called.
 */
public class SysEnvConfigSource extends ConfigSource {

    /**
     * Constructs a new <code>SysEnvConfigSource</code> with priority {@value #DEFAULT_PRIORITY}.
     */
    public SysEnvConfigSource() {
        this(DEFAULT_PRIORITY);
    }

    /**
     * Constructs a new <code>SysEnvConfigSource</code> with the given priority.
     *
     * @param priority
     *         the priority of this <code>SysEnvConfigSource</code>
     */
    public SysEnvConfigSource(int priority) {
        super(priority);
    }

    @Override
    protected Optional<String> getMapping(String key) {
        String res = System.getenv(key);

        if (res == null) {
            return Optional.empty();
        } else {
            return Optional.of(res);
        }
    }
}
