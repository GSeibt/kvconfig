/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Georg Seibt
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.uni_passau.fim.seibt.kvconfig.sources;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.Properties;

/**
 * A <code>ConfigSource</code> backed by a <code>Properties</code> instance.
 */
public class PropFileConfigSource extends ConfigSource {

    private Properties properties;

    /**
     * Constructs a new <code>PropFileConfigSource</code> with priority {@value #DEFAULT_PRIORITY} backed by the given
     * <code>Properties</code> instance.
     *
     * @param properties
     *         the <code>Properties</code> file to use
     */
    public PropFileConfigSource(Properties properties) {
        this(DEFAULT_PRIORITY, properties);
    }

    /**
     * Constructs a new <code>PropFileConfigSource</code> with the specified <code>priority</code> that is backed by
     * the
     * given <code>Properties</code> instance.
     *
     * @param priority
     *         the priority of this <code>ConfigSource</code>
     * @param properties
     *         the <code>Properties</code> instance to use
     */
    public PropFileConfigSource(int priority, Properties properties) {
        super(priority);
        this.properties = properties;
    }

    /**
     * Constructs a new <code>PropFileConfigSource</code> with priority {@value #DEFAULT_PRIORITY} backed by a
     * <code>Properties</code> instance red from the given <code>File</code>.
     *
     * @param propertiesFile
     *         the <code>File</code> in which the <code>Properties</code> are stored
     *
     * @throws IOException
     *         if an <code>IOException</code> occurs while reading the <code>File</code>
     */
    public PropFileConfigSource(File propertiesFile) throws IOException {
        this(DEFAULT_PRIORITY, propertiesFile);
    }

    /**
     * Constructs a new <code>PropFileConfigSource</code> with the specified priority that is backed by a
     * <code>Properties</code> instance red from the given <code>File</code>.
     *
     * @param priority
     *         the priority of this <code>PropFileConfigSource</code>
     * @param propertiesFile
     *         the <code>File</code> in which the <code>Properties</code> are stored
     *
     * @throws IOException
     *         if an <code>IOException</code> occurs while reading the <code>File</code>
     */
    public PropFileConfigSource(int priority, File propertiesFile) throws IOException {
        super(priority);
        properties = loadProperties(propertiesFile);
    }

    /**
     * Constructs a <code>Properties</code> instance from the given <code>File</code>. Assumes UTF-8 encoding.
     *
     * @param propertiesFile
     *         the <code>File</code> to read
     *
     * @return the resulting <code>Properties</code> instance
     *
     * @throws IOException
     *         if an <code>IOException</code> occurs while reading the <code>File</code>
     */
    private Properties loadProperties(File propertiesFile) throws IOException {
        Properties p = new Properties();

        Charset cs = StandardCharsets.UTF_8;
        try (Reader r = new InputStreamReader(new BufferedInputStream(new FileInputStream(propertiesFile)), cs)) {
            p.load(r);
        }

        return p;
    }

    @Override
    protected Optional<String> getMapping(String key) {
        String res = properties.getProperty(key);

        if (res == null) {
            return Optional.empty();
        } else {
            return Optional.of(res);
        }
    }
}
