/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Georg Seibt
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.uni_passau.fim.seibt.kvconfig.sources;

import java.util.Optional;

/**
 * A <code>ConfigSource</code> represents a repository of key/value pairs which can be added to a <code>Config</code>
 * instance. Every <code>ConfigSource</code> has a priority value that is used by the <code>Config</code> to determine
 * the order in which the added <code>ConfigSource</code>s are to be queried. Higher priority values mean
 * that the <code>ConfigSource</code> will be queried sooner.
 */
public abstract class ConfigSource implements Comparable<ConfigSource> {

    protected static final int DEFAULT_PRIORITY = 0;

    private int priority;
    private String prefix;
    private String suffix;

    /**
     * Constructs a new <code>ConfigSource</code> with priority {@value #DEFAULT_PRIORITY}.
     */
    public ConfigSource() {
        this(DEFAULT_PRIORITY);
    }

    /**
     * Constructs a new <code>ConfigSource</code> with the given priority.
     *
     * @param priority
     *         the priority of this <code>ConfigSource</code>
     */
    public ConfigSource(int priority) {
        this.priority = priority;
    }

    /**
     * Constructs a new <code>ConfigSource</code> with the given priority. <code>prefix</code> and <code>suffix</code>
     * are <code>String</code>s which will be added to any key given to the {@link #get(String)} method before searching
     * for a mapping.
     *
     * @param priority
     *         the priority of this <code>ConfigSource</code>
     * @param prefix
     *         the prefix for the keys (may be <code>null</code>)
     * @param suffix
     *         the suffix for the keys (may be <code>null</code>)
     */
    public ConfigSource(int priority, String prefix, String suffix) {
        this.priority = priority;
        this.prefix = prefix;
        this.suffix = suffix;
    }

    /**
     * Optionally returns the mapped value for the given <code>key</code> in this <code>ConfigSource</code>.
     * If no mapping exists an empty <code>Optional</code> is returned. If prefix or suffix is set the given
     * <code>key</code> will be modified appropriately.
     *
     * @param key
     *         the key to return the value for
     *
     * @return optionally the mapped value
     */
    public Optional<String> get(String key) {
        if (prefix == null && suffix == null) {
            return getMapping(key);
        } else {
            return getMapping(String.format("%s%s%s", (prefix != null) ? prefix : "", key, (suffix != null) ? suffix : ""));
        }
    }

    /**
     * Optionally returns the mapped value for the given <code>key</code> in this <code>ConfigSource</code>.
     * If no mapping exists an empty <code>Optional</code> is returned.
     *
     * @param key
     *         the key to return the value for
     *
     * @return optionally the mapped value
     */
    protected abstract Optional<String> getMapping(String key);

    @Override
    public int compareTo(ConfigSource o) {
        return Integer.compare(priority, o.priority);
    }

    /**
     * Returns the currently set prefix.
     *
     * @return the prefix
     */
    public String getPrefix() {
        return prefix;
    }

    /**
     * Sets the prefix to the given value.
     *
     * @param prefix
     *         the new prefix
     */
    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    /**
     * Returns the currently set suffix.
     *
     * @return the suffix
     */
    public String getSuffix() {
        return suffix;
    }

    /**
     * Sets the suffix to the given value.
     *
     * @param suffix
     *         the new suffix
     */
    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }
}
