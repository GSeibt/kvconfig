/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Georg Seibt
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.uni_passau.fim.seibt.kvconfig;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import de.uni_passau.fim.seibt.kvconfig.sources.ConfigSource;

/**
 * A <code>Config</code> is used to access the collected configuration options from all added
 * <code>ConfigSource</code>s. The values for a given configuration key can be retrieved using the {@link #get(String)}
 * function. Similar functions for retrieving the value parsed as a type other than <code>String</code> are
 * also provided. When a configuration value is requested, the added <code>ConfigSource</code>s will be queried in
 * descending order of their priority. The first non-empty <code>Optional</code> returned for a key from a
 * <code>ConfigSource</code> is then returned to the caller of the 'get' function.
 */
public class Config {

    private static Function<? super String, Optional<Integer>> toInteger = s -> {
        try {
            return Optional.of(Integer.parseInt(s));
        } catch (NumberFormatException e) {
            return Optional.empty();
        }
    };

    private static Function<? super String, Optional<Long>> toLong = s -> {
        try {
            return Optional.of(Long.parseLong(s));
        } catch (NumberFormatException e) {
            return Optional.empty();
        }
    };

    private static Function<? super String, Optional<Float>> toFloat = s -> {
        try {
            return Optional.of(Float.parseFloat(s));
        } catch (NumberFormatException e) {
            return Optional.empty();
        }
    };

    private static Function<? super String, Optional<Double>> toDouble = s -> {
        try {
            return Optional.of(Double.parseDouble(s));
        } catch (NumberFormatException e) {
            return Optional.empty();
        }
    };

    private static Function<? super String, Optional<Boolean>> toBoolean = s -> Optional.of(Boolean.parseBoolean(s));

    private static Function<? super String, Optional<File>> toFile = s -> Optional.of(new File(s));

    private List<ConfigSource> sources;

    /**
     * Constructs a new <code>Config</code> instance with no attached <code>ConfigSource</code>s.
     */
    public Config() {
        sources = new ArrayList<>();
    }

    /**
     * Adds a <code>ConfigSource</code> to the list of sources this <code>Config</code> considers when a 'get' function
     * is called.
     *
     * @param source
     *         the <code>ConfigSource</code> to add
     */
    public void addSource(ConfigSource source) {
        sources.add(source);
        Collections.sort(sources, Collections.reverseOrder());
    }

    /**
     * Removes the given <code>ConfigSource</code> from the list of sources this <code>Config</code> considers when a
     * 'get' function is called.
     *
     * @param source
     *         the <code>ConfigSource</code> to remove
     */
    public void removeSource(ConfigSource source) {
        sources.remove(source);
    }

    /**
     * Optionally returns the value associated with the given <code>key</code>. If no <code>ConfigSource</code>
     * contains a mapping for <code>key</code> and empty <code>Optional</code> is returned. Otherwise the mapping
     * from the <code>ConfigSource</code> with the highest priority is returned. If several <code>ConfigSource</code>s
     * with the same (highest) priority contain a mapping for <code>key</code> it is unspecified which value is
     * returned.
     *
     * @param key
     *         the key to return the value for
     *
     * @return optionally the value
     */
    public Optional<String> get(String key) {
        Optional<String> res = Optional.empty();

        for (ConfigSource source : sources) {
            res = source.get(key);

            if (res.isPresent()) {
                break;
            }
        }

        return res;
    }

    /**
     * Optionally returns the result of {@link #get(String)} for the given key parsed by the given <code>parser</code>.
     *
     * @param key
     *         the key to return the parsed value for
     * @param parser
     *         the parser to use
     * @param <T>
     *         the resulting type after applying the parser
     *
     * @return optionally the parsed value
     */
    public <T> Optional<T> get(String key, Function<? super String, Optional<T>> parser) {
        return get(key).flatMap(parser);
    }

    /**
     * Optionally returns the result of {@link #get(String)} parsed as an <code>Integer</code> using
     * {@link Integer#parseInt(String)}. If a <code>NumberFormatException</code> occurs an empty <code>Optional</code>
     * is returned.
     *
     * @param key
     *         the key to return the parsed value for
     *
     * @return optionally the value as an <code>Integer</code>
     */
    public Optional<Integer> getInteger(String key) {
        return get(key, toInteger);
    }

    /**
     * Optionally returns the result of {@link #get(String)} parsed as a <code>Long</code> using
     * {@link Long#parseLong(String)}. If a <code>NumberFormatException</code> occurs an empty <code>Optional</code>
     * is returned.
     *
     * @param key
     *         the key to return the parsed value for
     *
     * @return optionally the value as a <code>Long</code>
     */
    public Optional<Long> getLong(String key) {
        return get(key, toLong);
    }

    /**
     * Optionally returns the result of {@link #get(String)} parsed as a <code>Float</code> using
     * {@link Float#parseFloat(String)}. If a <code>NumberFormatException</code> occurs an empty <code>Optional</code>
     * is returned.
     *
     * @param key
     *         the key to return the parsed value for
     *
     * @return optionally the value as a <code>Float</code>
     */
    public Optional<Float> getFloat(String key) {
        return get(key, toFloat);
    }

    /**
     * Optionally returns the result of {@link #get(String)} parsed as a <code>Double</code> using
     * {@link Double#parseDouble(String)}. If a <code>NumberFormatException</code> occurs an empty
     * <code>Optional</code> is returned.
     *
     * @param key
     *         the key to return the parsed value for
     *
     * @return optionally the value as an <code>Double</code>
     */
    public Optional<Double> getDouble(String key) {
        return get(key, toDouble);
    }

    /**
     * Optionally returns the result of {@link #get(String)} parsed as a <code>Boolean</code> using
     * {@link Boolean#parseBoolean(String)}.
     *
     * @param key
     *         the key to return the parsed value for
     *
     * @return optionally the value as an <code>Boolean</code>
     */
    public Optional<Boolean> getBoolean(String key) {
        return get(key, toBoolean);
    }

    /**
     * Optionally returns the result of {@link #get(String)} used as the pathname for a <code>File</code>.
     *
     * @param key
     *         the key to use the value of
     *
     * @return optionally the resulting <code>File</code>
     */
    public Optional<File> getFile(String key) {
        return get(key, toFile);
    }

    /**
     * Optionally returns the result of {@link #get(String)} used as the pathname for a <code>File</code>.
     * If the resulting <code>File</code> does not exist an empty <code>Optional</code> is returned.
     *
     * @param key
     *         the key to use the value of
     *
     * @return optionally the resulting <code>File</code>
     */
    public Optional<File> getExistingFile(String key) {
        return get(key, toFile).filter(File::exists);
    }

    /**
     * Optionally returns the result of {@link #get(String)} parsed as the requested <code>Enum</code> using
     * {@link Enum#valueOf(Class, String)}. If the enum does not contain a constant with the name <code>key</code>
     * is mapped to, an empty <code>Optional</code> is returned.
     *
     * @param key
     *         the key to use the value of
     * @param enumType
     *         the <code>Class</code> object of the enum type from which to return a constant
     * @param <T>
     *         the type of the enum whose constant is to be returned
     * @return optionally the resulting <code>Enum</code>
     */
    public <T extends Enum<T>> Optional<T> getEnum(String key, Class<T> enumType) {
        return get(key, val -> {

            try {
                return Optional.of(T.valueOf(enumType, val));
            } catch (IllegalArgumentException e) {
                return Optional.empty();
            }
        });
    }
}
